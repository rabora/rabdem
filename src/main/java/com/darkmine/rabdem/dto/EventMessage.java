package com.darkmine.rabdem.dto;

import java.io.Serializable;

public class EventMessage implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
