package com.darkmine.rabdem.event.consumer;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EventConsumerConfig {

    @Bean("inboundEventQueue")
    public Queue queue() {
        return new Queue("inboundEventQueue");
    }

    @Bean("inboundSharedEventQueue")
    public Queue sharedQueue() {
        return new Queue("inboundSharedEventQueue");
    }

    @Bean
    public Binding binding(@Qualifier("inboundEventQueue") Queue queue,@Qualifier("eventExchange") Exchange eventExchange) {
        return BindingBuilder
                .bind(queue)
                .to(eventExchange)
                .with("event.*")
                .noargs();
    }

}
