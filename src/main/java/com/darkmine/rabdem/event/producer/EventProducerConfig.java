package com.darkmine.rabdem.event.producer;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EventProducerConfig {

    @Bean("eventExchange")
    public TopicExchange eventExchange() {
        return new TopicExchange("eventExchange");
    }

}
