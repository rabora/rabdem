package com.darkmine.rabdem.event.producer;

import com.darkmine.rabdem.dto.EventMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class EventProducer {
    private static final Logger logger = LoggerFactory.getLogger(EventProducer.class);
    private final RabbitTemplate rabbitTemplate;
    private final Exchange exchange;

    public EventProducer(RabbitTemplate rabbitTemplate,@Qualifier("eventExchange") Exchange exchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.exchange = exchange;
    }

    public void signal(String routingKey,EventMessage message) {
        logger.info("Sending signal to {} with {}",routingKey,message);
        rabbitTemplate.convertAndSend(exchange.getName(), routingKey, message);
    }
}
