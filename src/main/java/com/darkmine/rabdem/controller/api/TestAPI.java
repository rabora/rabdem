package com.darkmine.rabdem.controller.api;

import com.darkmine.rabdem.dto.EventMessage;
import com.darkmine.rabdem.event.producer.EventProducer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class TestAPI {

    private final EventProducer eventProducer;

    public TestAPI(EventProducer eventProducer) {
        this.eventProducer = eventProducer;
    }

    @GetMapping("/api/test/ping")
    public String ping() {
        long n = System.currentTimeMillis();
        EventMessage message = createEventMessage(""+n);
        message.setName("Ping " + System.currentTimeMillis());
        eventProducer.signal("event.ping", message);
        return message.getName();
    }

    @GetMapping("/api/test/event/{text}")
    public String event(@PathVariable("text") String text) {
        long n = System.currentTimeMillis();
        EventMessage message = createEventMessage(""+n);
        message.setName("Event " + text + " " + n);
        eventProducer.signal(String.format("event.%s",text), message);
        return message.getName();
    }

    @GetMapping("/api/test/bulk")
    public String bulk() {
        for(int i = 0 ; i < 1000 ; i++ ) {
            UUID id = UUID.randomUUID();
            EventMessage message = createEventMessage("Bulk One " + id);
            eventProducer.signal("bulk.one", message);
        }
        for(int i = 0 ; i < 1 ; i++ ) {
            UUID id = UUID.randomUUID();
            EventMessage message = createEventMessage("Bulk Two " + id);
            eventProducer.signal("bulk.two", message);
        }
        return "OK";
    }

    private static EventMessage createEventMessage(String name) {
        EventMessage message = new EventMessage();
        message.setName(name);
        return message;
    }

}
