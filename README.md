# Rabdem (Rabbit Demo)

This project contains some sample code to use Rabbit topics for dealing with events.

# Testing

It is exposes a TestAPI to allow you to raise events to see what happens. Run the DemoApplication and try sticking these into your browser:

http://localhost:8080/api/test/ping

http://localhost:8080/api/test/event/hello

http://localhost:8080/api/test/event/hello.one

http://localhost:8080/api/test/event/hello.two

http://localhost:8080/api/test/event/hello.three

http://localhost:8080/api/test/event/hello.four

http://localhost:8080/api/test/event/hello.five

# Key classes

The **EventConsumer** is the place to witness events arriving. There are comments about how each pattern is working.

The **EventProducer** is how to send events.

The **EventMessage** is the payload. This could be replaced with JSON (by declaring a Jackson2JsonMessageConverter bean), and
another pattern is to make it abstract, where each subclass would be able to compute its routing key. For example:

```java
public class CarrierReadyEventMsg extends AbstractEventMsg {

    private String carrierId;

    public CarrierReadyEventMsg() {}

    public CarrierReadyEventMsg(String carrierId) {
        this.carrierId = carrierId;
    }

    @Override
    public String getRoutingKey() {
        return String.format("carrier.%s.ready", getCarrierId());
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }
}
```

